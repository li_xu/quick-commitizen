# quick-commitizen


husky问题，基于原作者的修改
https://github.com/we125182/quick-commitizen

This tool is for quick config on [commitizen](https://github.com/commitizen/cz-cli). Setting your repo to use [AngularJS's commit message convention](https://github.com/angular/angular.js/blob/master/DEVELOPERS.md#-git-commit-guidelines) also known as [conventional-changelog](https://github.com/conventional-changelog/conventional-changelog).

```
// commit msg format:
<type>(<scope>): <subject>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
```

[![current version](https://img.shields.io/npm/v/quick-commitizen.svg?style=flat-square)](https://www.npmjs.com/package/quick-commitizen)

## Install

```
npm install -g quick-h5-commitizen
```

## Usage

```
// All you need to do is
h5git
```

The above command does:

1. Installs `commitizen cz-conventional-changelog @commitlint/cli husky conventional-changelog-cli` module
2. Saves it to devDependencies
3. adds belowe keys to the root of your package.json

```
{
  ...

  "config": {
      "commitizen": {
        "path": "./node_modules/cz-conventional-changelog"
      }
    },
    "husky": {
      "hooks": {
        "commit-msg": "commitlint -E HUSKY_GIT_PARAMS"
      }
    },
    "scripts": {
      "cz": 'npm run changelog && git add . && git-cz && git push origin HEAD',
      "changelog": "conventional-changelog -p angular -i CHANGELOG.md -s && git add CHANGELOG.md"
    }
}
```

4. adds the `commitlint.config.js` to root directory.

> Now, you can use `npm run std-cm` instead `git commit`. you'll be you'll be prompted to fill in any required fields and your commit messages will be formatted according to the standards defined by project maintainers. [details](https://github.com/commitizen/cz-cli). like:

![image](https://raw.githubusercontent.com/commitizen/cz-cli/master/meta/screenshots/add-commit.png)

when you release a version, you can run `npm run changelog` to generate CHANGELOG.md that contains your commit messages.

## Related projects

- [conventional-changelog](https://github.com/conventional-changelog/conventional-changelog) – Generate a changelog from conventional commit history
- [commitlint](https://github.com/marionebl/commitlint) - Lint commit messages

## git-cz相关
```
#指定commit的类型，约定了feat、fix两个主要type，以及docs、style、build、refactor、revert五个特殊type
? **Select the type of change that you're committing:** fix:   A bug fix

#用于描述改动的范围，格式为项目名/模块名
? **What is the scope of this change (e.g. component or file name): (press enter t**
**o skip)** index.html

#对改动进行简短的描述
? **Write a short, imperative tense description of the change (max 83 chars):**
 (11) add a blank

#对改动进行长的描述
? **Provide a longer description of the change: (press enter to skip)**

#是破坏性的改动吗
? **Are there any breaking changes?** No

#影响了哪个issue吗，如果选是，接下来要输入issue号
? **Does this change affect any open issues?** No
```

## git commit msg 规范
注意空格
``` sh
git commit -m 'feat: add feature'
```
* feat: 表示新增了一个功能
* fix: 表示修复了一个 bug
* docs: 表示只修改了文档
* style: 表示修改格式、书写错误、空格等不影响代码逻辑的操作
* refactor: 表示修改的代码不是新增功能也不是修改 bug，比如代码重构
* perf: 表示修改了提升性能的代码
* test: 表示修改了测试代码
* build: 表示修改了编译配置文件
* chore: 无 src 或 test 的操作
* revert: 回滚操作
